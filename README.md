# Extract email and phone number from resume

Supported formats : .PDF and .Docx



## How to use

Place all the pdf or .docs supported resumes in folder and pass directory of the resumes as argument to the .py file\

For example `resume_info_extract.py D:/path-to-resumes`\

Find the excel file `outside.xlsx` in the same directory where .py is saved to get the content of resumes
