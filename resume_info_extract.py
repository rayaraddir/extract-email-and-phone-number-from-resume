# -*- coding: utf-8 -*-

import docx2txt
import re
import os
from openpyxl import Workbook
from pdfminer import high_level
import sys
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
from pdfminer.pdfpage import PDFPage
from io import StringIO

def convert_pdf_to_txt(path):
    rsrcmgr = PDFResourceManager()
    retstr = StringIO()
    codec = 'utf-8'
    laparams = LAParams()
    device = TextConverter(rsrcmgr, retstr, laparams=laparams)
    fp = open(path, 'rb')
    interpreter = PDFPageInterpreter(rsrcmgr, device)
    password = ""
    maxpages = 0
    caching = True
    pagenos=set()

    for page in PDFPage.get_pages(fp, pagenos, maxpages=maxpages, password=password,caching=caching, check_extractable=True):
        interpreter.process_page(page)

    text = retstr.getvalue()

    fp.close()
    device.close()
    retstr.close()
    return text


workbk=Workbook()
excel=workbk.active
excel.append(['Email ID','Mobile number', 'Resume file'])

dirs = sys.argv[1]

dir_lst = os.listdir(dirs)


cnt = 0
discard_list = []
for resumes in dir_lst :
    text_file = ''
    print( ' - ' + str(cnt + 1) + '/' + str(len(dir_lst)) + '  --  getting details from ' + str(resumes))
    if resumes.endswith('.pdf'):
        path_resume = os.path.join(dirs,resumes)
        
        '''
        pdfFileObj = open(path_resume, 'rb')
        pdfReader = PyPDF2.PdfFileReader(pdfFileObj) 
            
        # printing number of pages in pdf file 
        number_of_pages = pdfReader.numPages
       
        for page_number in range(number_of_pages):  
            page = pdfReader.getPage(page_number)
            page_content = page.extractText()
            text_file = text_file  + page_content
        '''
        try :
            
            text_file = convert_pdf_to_txt(path_resume)
        except Exception as err:    
            text_file = '' 
            #discard_list.append(resumes)
            
    
        
        email_id_list = re.findall(r"[A-Za-z0-9\.\+_]+@[a-z0-9\.\-+_]+\.[a-z]+", text_file)
        try :
            email_id = str(email_id_list[0])
        except Exception as err: 
            email_id = ''
            #discard_list.append(resumes)
        
        #pattern = re.compile(r'[a-zA-Z0-9-\.]+@[a-zA-Z-\.]*\.(com|edu|net)')
        #matches = pattern.finditer(text_file)
        
        try :
            mobile_number = re.findall("\d{12}" , text_file)
            if(len(mobile_number) > 0):
                mobile = str(mobile_number[0])[2:]
                
                if(not mobile_number[0].startswith('91')) :
                    mobile_number_10 = re.findall("\d{10}" , text_file)
                    mobile = str(mobile_number_10[0])
            else:
                mobile_number_20 = re.findall("\d{10}" , text_file)
                if(len(mobile_number_20) > 0):
                    mobile = str(mobile_number_20[0])
                else:
                    mobile_number_55 = re.findall("\d{5}[ -.]?\d{5}" , text_file)
                    if(len(mobile_number_55) > 0):
                        mobile = str(mobile_number_55[0])[0:5] + str(mobile_number_55[0])[6:11]
                    else:
                        mobile_number_334 = re.findall("\d{3}[ -.]?\d{3}[ -.]?\d{4}" , text_file)     
                        mobile = str(mobile_number_334[0])[0:3] + str(mobile_number_334[0])[4:7] + str(mobile_number_334[0])[8:12]
            
            #print(mobile)
        except Exception as err:
            mobile = ''
        excel.append([email_id, mobile, resumes])
        cnt = cnt +1 
    elif resumes.endswith('.docx') or resumes.endswith('.docx'):
        path_resume = os.path.join(dirs,resumes)
        
        text_file = docx2txt.process(path_resume)
        
        email_id_list = re.findall(r"[A-Za-z0-9\.\+_]+@[a-z0-9\.\-+_]+\.[a-z]+", text_file)
        try :
            email_id = email_id_list[0]
        except Exception as err: 
            email_id = ''
            #discard_list.append(resumes)
        try :
            mobile_number = re.findall("\d{12}" , text_file)
            if(len(mobile_number) > 0):
                mobile = str(mobile_number[0])[2:]
                if(not mobile_number[0].startswith('91')) :
                    mobile_number_10 = re.findall("\d{10}" , text_file)
                    mobile = str(mobile_number_10[0])
            else:
                mobile_number_20 = re.findall("\d{10}" , text_file)
                if(len(mobile_number_20) > 0):
                    mobile = str(mobile_number_20[0])
                else:
                    mobile_number_55 = re.findall("\d{5}[ -.]?\d{5}" , text_file)
                    if(len(mobile_number_55) > 0):
                        mobile = str(mobile_number_55[0])[0:5] + str(mobile_number_55[0])[6:11]
                    else:
                        mobile_number_334 = re.findall("\d{3}[ -.]?\d{3}[ -.]?\d{4}" , text_file)     
                        mobile = str(mobile_number_334[0])[0:3] + str(mobile_number_334[0])[4:7] + str(mobile_number_334[0])[8:12]
            
            #print(mobile)
        except Exception as err:
            mobile = ''

        excel.append([email_id, mobile, resumes])
                 
        cnt = cnt +1
    else :
        discard_list.append(resumes)
        cnt = cnt +1

for resumes in discard_list :
    excel.append(['', '', resumes])
workbk.save('OUTSIDE.xlsx')
